<?php

namespace BeachApiBundle\Controller;

use BeachBundle\Document\Beach;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BeachApiController extends Controller
{
    public function getJsonAction(Request $request)
    {
        $dm = $this->get("doctrine_mongodb")->getManager();
        $beachRepo = $dm->getRepository('BeachBundle:Beach');
        /**
         * @var Beach $lastModifiedData
         */
        $lastModifiedData = $beachRepo
            ->createQueryBuilder()
            ->find()
            ->sort("lastUpdate", -1)->sort('created', -1)
            ->limit(1)
            ->getQuery()
            ->getSingleResult();

        $lastModifiedData = $lastModifiedData ? $lastModifiedData : new Beach();

        /**
         * @var \MongoTimestamp|null $lastUpdate
         */
        $lastUpdate = isset($lastModifiedData->getLastUpdate()->sec) ? $lastModifiedData->getLastUpdate()->sec : null;

        /**
         * @var \MongoTimestamp|null $created
         */
        $created = isset($lastModifiedData->getCreated()->sec) ? $lastModifiedData->getCreated()->sec : null;

        $lastChanges = $lastUpdate > $created ? $lastUpdate : $created;

        $response = new Response();
        $response->setPublic();
        $response->setSharedMaxAge(86400);
        $response->headers->set('Content-Type', 'application/json');
        $response->setLastModified(new \DateTime(date("Y-m-d H:i:s", $lastChanges)));

        if ($response->isNotModified($request)) {
            return $response;
        }

        $countries = $beachRepo
            ->createQueryBuilder()
            ->distinct('country')
            ->getQuery()
            ->execute()
            ->toArray();

        $beachesByCountry = [];

        foreach ($countries as &$country) {
            $beachesByCountry[$country] = $beachRepo->findBy(['country' => $country]);
        }

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($beachesByCountry, 'json');

        $response->setLastModified(new \DateTime());
        $response->setContent($json);

        return $response;
    }
}