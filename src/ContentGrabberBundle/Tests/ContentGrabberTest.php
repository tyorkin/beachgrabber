<?php
namespace ContentGrabberBundle\Tests;


use BeachParserBundle\BeachParser;
use ContentGrabberBundle\ContentGrabber;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContentGrabberTest extends KernelTestCase
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    /**
     * @dataProvider countries
     */
    public function testGrabberNotNullForSearchUrl($country) {

        $grabber = new ContentGrabber();
        $grabber->setUrl("https://www.beach-inspector.com/en/search/strand/?country=$country");

        $content = $grabber->getContent();

        $this->assertNotNull(
            $content
        );

        return $content;
    }

    /**
     * @dataProvider beachesUrls
     */
    public function testGrabberNotNullForBeachUrl($url) {
        $grabber = $this->container->get('content_grabber');
        $grabber->setUrl($url);
        $html = $grabber->getContent();

        $this->assertNotNull(
            $html
        );

        return $html;
    }


    /**
     * @depends testGrabberNotNullForBeachUrl
     */
    public function testParser($html) {
        $parser = $this->container->get('beach_parser');
        $parser->setHtml($html);

        $result = $parser->getResult();

        $this->assertNotNull(
            $result
        );
    }

    public function countries() {
        return [
            ['Spain'],
            ['Oman'],
        ];
    }

    public function beachesUrls() {
        return [
            ['https://www.beach-inspector.com/en/tenerife-beaches/beach-el_toscal/playa_de_los_roques'],
            ['https://www.beach-inspector.com/en/fuerteventura-beaches/beach-barranco_del_mal_nombre/tierra_dorada'],
            ['https://www.beach-inspector.com/en/oman-beaches/beach-sur/turtle_beach_resort']
        ];
    }
}
