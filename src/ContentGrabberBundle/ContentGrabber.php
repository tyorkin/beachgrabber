<?php

namespace ContentGrabberBundle;

use GuzzleHttp\Client;

class ContentGrabber
{
    private $url;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getContent()
    {
        $client = new Client();
        $response = $client->request('GET', $this->url);

        return $response->getStatusCode() == 200 ? $response->getBody()->getContents() : null;
    }
}