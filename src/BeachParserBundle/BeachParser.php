<?php

namespace BeachParserBundle;

use BeachBundle\Document\AgeRange;
use BeachBundle\Document\Beach;
use BeachBundle\Document\BeachDirection;
use BeachBundle\Document\BeachFact;
use BeachBundle\Document\CarParkingSituation;
use BeachBundle\Document\Fact;
use BeachBundle\Document\Food;
use BeachBundle\Document\LifestyleInfo;
use BeachBundle\Document\Nationality;
use BeachBundle\Document\Photo;
use BeachBundle\Document\Risk;
use BeachBundle\Document\UsageTime;
use BeachBundle\Document\WaterSport;
use Symfony\Component\DomCrawler\Crawler;

class BeachParser
{
    private $html;
    private $crawler;

    /**
     * @return mixed
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html)
    {
        $this->html = $html;
        $this->crawler = new Crawler($this->html);
    }

    public function getResult()
    {
        $beach = new Beach();
        $beach->setPhotos($this->getPhotos());
        $beach->setBeachFacts($this->getBeachFacts());
        $beach->setLifestyleInfo($this->getLifestyleInfo());
        $beach->setNationalities($this->getNationalities());
        $beach->setUsageTimes($this->getUsageTimes());
        $beach->setAgeRanges($this->getAgeRanges());
        $beach->setDirectionsToTheBeach($this->getDirectionsToTheBeach());
        $beach->setCarParkingSituation($this->getCarParkingSituation());
        $beach->setRisks($this->getRisks());
        $beach->setFood($this->getFood());
        $beach->setWaterSports($this->getWaterSports());

        return $beach;
    }

    public function getPhotos()
    {
        preg_match('/<div id="beachGallery">.*<script>(.+)<\/script>.*/simU', $this->html, $photoWrap);

        if (!is_array($photoWrap) || count($photoWrap) <= 0) {
            return [];
        }

        preg_match_all('/bi.currentBeachImages.push\(\{.*src: \'(.+)\'/simU', $photoWrap[1], $photos);

        if (!is_array($photos)) {
            return [];
        }
        $result = [];
        foreach ($photos[1] as $photo) {
            $photo = $this->trim($photo);
            $result[] = (new Photo())->setUrl($photo);
        }

        return $result;
    }

    public function getBeachFacts()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $result = $crawler
            ->filter('div.beach-facts')
            ->filter('h5')
            ->each(function ($node, $i) use ($crawler) {
                $facts = $crawler
                    ->filter('div.beach-facts')
                    ->filter('div.fact-list')
                    ->eq($i)
                    ->filter('div.col-xxs-6')
                    ->each(function ($node) {
                        $altText = $node
                            ->filter('img')
                            ->attr('alt');
                        $name = $node
                            ->filter('span')
                            ->text();

                        $name = $this->trim($name);
                        $rating = $this->calculateRatingByText($altText);

                        return (new Fact())->setName($name)->setValue($rating);
                    });

                $name = $this->trim($node->text());

                return (new BeachFact())->setFacts($facts)->setName($name);
            });

        return $result;
    }

    public function getLifestyleInfo()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $result = $crawler
            ->filter('#lifestyle')
            ->filter('div.row-trenner')
            ->filter('div.col-faktor')
            ->each(function ($node, $i) {
                $name = $node
                    ->text();
                $current = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuenow');
                $max = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuemax');

                $name = $this->trim($name);
                $rating = $this->calculateRating($max, $current);

                return (new LifestyleInfo())->setName($name)->setRating($rating);
            });

        return $result;
    }

    public function getNationalities()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $result = $crawler
            ->filter('div.col-nationalitaet')
            ->filter('div.row-trenner')
            ->filter('div.col-faktor')
            ->each(function ($node, $i) {
                $name = $node
                    ->text();
                $current = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuenow');
                $max = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuemax');

                $name = $this->trim($name);
                $rating = $this->calculateRating($max, $current);

                return (new Nationality())->setName($name)->setRating($rating);
            });

        return $result;
    }

    public function getUsageTimes()
    {
        $months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'
        ];
        preg_match('/<script>.*"BeachCrowdedChartData":\[(.+)\].*<\/script>/simU', $this->html, $chartData);

        if (!is_array($chartData) || count($chartData) <= 0) {
            return [];
        }
        $data = explode(',', $chartData[1]);
        $result = [];
        foreach ($months as $key => $month) {
            $amount = isset($data[$key]) ? $data[$key] : 0;
            $result[] = (new UsageTime())->setName($month)->setAmountOfPeople($amount);
        }

        return $result;
    }

    public function getAgeRanges()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $result = $crawler
            ->filter('div.col-altersverteilung')
            ->filter('div.row-trenner')
            ->filter('div.col-faktor')
            ->each(function ($node, $i) {
                $name = $node
                    ->text();
                $current = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuenow');
                $max = $node
                    ->parents()
                    ->filter('div.progress-bar')
                    ->eq($i)
                    ->attr('aria-valuemax');

                $name = $this->replaceString($name, 'years');
                $rating = $this->calculateRating($max, $current);

                return (new AgeRange())->setName($name)->setRating($rating);
            });

        return $result;
    }

    public function getDirectionsToTheBeach()
    {
        $crawler = $this->crawler;
        $index = 0;
        /**
         * @var Crawler $crawler
         */
        $wrap = $crawler
            ->filter('div.sidebar')
            ->filter('div.panel')
            ->filter('div.panel-body')
            ->filter('div.show_more_container')
            ->filter('div.show_more_div')
            ->eq($index);

        $div = $wrap
            ->filter('div.nopadding-vert');

        $description = $div->count() > 0 ? $div->text() : '';

        $ratings = $wrap
            ->filter('div.progress-bar')
            ->each(function ($node) {
                $max = $node->attr('aria-valuemax');
                $current = $node->attr('aria-valuenow');
                return $this->calculateRating($max, $current);
            });

        $description = $this->trim($description);
        $ratingDifficultyOfJourney = isset($ratings[0]) ? $ratings[0] : 0;
        $ratingDifficultyParkingToBeach = isset($ratings[1]) ? $ratings[1] : 0;

        $result[] = (new BeachDirection())->setDetailedDescription($description)->setDifficultyOfJourney($ratingDifficultyOfJourney)->setDifficultyParkingToBeach($ratingDifficultyParkingToBeach);

        return $result;
    }

    public function getCarParkingSituation()
    {
        $crawler = $this->crawler;
        $index = 1;
        /**
         * @var Crawler $crawler
         */
        $wrap = $crawler
            ->filter('div.sidebar')
            ->filter('div.panel')
            ->filter('div.panel-body')
            ->filter('div.show_more_container')
            ->filter('div.show_more_div')
            ->eq($index);

        $description = '';

        if ($wrap->count() > 0) {
            preg_match('/(.+)<h5.*>/simU', $wrap->html(), $matches);

            if (is_array($matches)) {
                $description = $matches[1];
            }
        }
        $description = $this->trim($description);

        $ratings = $wrap
            ->filter('div.progress-bar')
            ->each(function ($node) {
                $max = $node->attr('aria-valuemax');
                $current = $node->attr('aria-valuenow');

                return $this->calculateRating($max, $current);
            });

        $description = $this->trim($description);
        $ratingDifficultyFindingParking = isset($ratings[0]) ? $ratings[0] : 0;

        $result = (new CarParkingSituation())->setParkingDescription($description)->setDifficultyFindingParking($ratingDifficultyFindingParking);

        return $result;
    }

    public function getRisks()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $result = $crawler
            ->filter('div.sidebar')
            ->filter('div.panel')
            ->filter('div.panel-body')
            ->filter('div.beach-danger')
            ->filter('ul.danger-list')
            ->filter('li')
            ->each(function ($node) {
                $name = $this->trim($node->text());

                return (new Risk())->setName($name);
            });

        return $result;
    }

    public function getFood()
    {
        $crawler = $this->crawler;
        /**
         * @var Crawler $crawler
         */
        $wrapDiv = $crawler
            ->filter('#gastronomie')
            ->filter('div.col-lineheight');
        $result = [];

        if($wrapDiv->count() > 0) {
            preg_match_all('/<strong class="auszeichnung_orange">(.+):<\/strong>(.+)<br>/simU', $wrapDiv->html(),
                $matches);

            if (!is_array($matches)) {
                return [];
            }


            foreach ($matches[1] as $key => $item) {
                $value = $matches[2][$key];
                $value = $this->trim($value);
                $name = $this->trim($item);

                $result[] = (new Food())->setName($name)->setValue($value);
            }
        }

        return $result;
    }

    public function getWaterSports()
    {
        $crawler = $this->crawler;
        $result = $crawler
            ->filter('#wassersport')
            ->filter('div.water-sports-conditions')
            ->filter('div.col-trenner')
            ->each(function ($node, $i) {
                $name = $node
                    ->filter('strong')
                    ->text();
                $rotate = $node
                    ->filter('g')
                    ->attr('transform');
                $rotate = $this->replaceString($rotate, 'rotate(');
                $max = 180;
                $current = explode(',', $rotate)[0];

                $name = $this->trim($name);
                $rating = $this->calculateRating($max, $max - $current);

                return (new WaterSport())->setName($name)->setRating($rating);
            });

        return $result;
    }

    private function calculateRatingByText($text = '')
    {
        $text = $this->trim($text);
        $rating = 0;
        if ('positive' === $text || 'yes' === $text) {
            $rating = 10;
        }

        return $rating;
    }

    private function calculateRating($max = 100, $current = 0)
    {
        $rating = 0;
        if (0 === $current) {
            return $rating;
        }

        $one = $max / 10; // 10 - Max number for customer requirement
        $rating = $current / $one;

        return round($rating);
    }

    private function trim($string)
    {
        return trim(preg_replace('/\s+/', ' ', $string));
    }

    private function replaceString($string, $replaceString)
    {
        return trim(str_replace($replaceString, '', $string));
    }
}