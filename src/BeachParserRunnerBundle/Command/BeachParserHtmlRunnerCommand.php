<?php

namespace BeachParserRunnerBundle\Command;

use BeachBundle\Document\Beach;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BeachParserHtmlRunnerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('beach_parser:run_html')
            ->setDescription('Beach parser');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $beaches = $dm->getRepository('BeachBundle:Beach')->findAll();

        foreach ($beaches as $beach) {
            $url = $this->getContainer()->getParameter('site_url').$beach->getUrl();
            $output->write("Processing url <comment>".$beach->getUrl()."...</comment>");
            $grabber = $this->getContainer()->get('content_grabber');
            $grabber->setUrl($url);
            $html = $grabber->getContent();
            $parser = $this->getContainer()->get('beach_parser');
            $parser->setHtml($html);

            $parsedBeach = $parser->getResult();

            $beach->setPhotos($parsedBeach->getPhotos());
            $beach->setBeachFacts($parsedBeach->getBeachFacts());
            $beach->setLifestyleInfo($parsedBeach->getLifestyleInfo());
            $beach->setNationalities($parsedBeach->getNationalities());
            $beach->setUsageTimes($parsedBeach->getUsageTimes());
            $beach->setAgeRanges($parsedBeach->getAgeRanges());
            $beach->setDirectionsToTheBeach($parsedBeach->getDirectionsToTheBeach());
            $beach->setCarParkingSituation($parsedBeach->getCarParkingSituation());
            $beach->setRisks($parsedBeach->getRisks());
            $beach->setFood($parsedBeach->getFood());
            $beach->setWaterSports($parsedBeach->getWaterSports());

            $dm->persist($beach);

            $output->write('<info>Complete.</info>');$output->writeln('');
        }
        $dm->flush();
    }

}