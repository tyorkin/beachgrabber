<?php

namespace BeachParserRunnerBundle\Command;

use BeachBundle\Document\Beach;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BeachParserApiRunnerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('beach_parser:run_api')
            ->setDescription('Beach parser');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $countries = $this->getCountries();

        $grabber = $this->getContainer()->get('content_grabber');
        $serializer = $this->getContainer()->get('jms_serializer');
        $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
        $siteSearchUrl = $this->getContainer()->getParameter('site_search_url');

        if(count($countries) <= 0) {
            $output->writeln("<error>No countries found!</error>");
        }

        $dm->getRepository('BeachBundle:Beach')->createQueryBuilder()->remove()->getQuery()->execute();

        foreach ($countries as $country) {
            $output->writeln("Parsing country <info>$country</info>...");
            $currentPage = 1;
            $totalPages = 1;

            while ($currentPage <= $totalPages) {
                $output->writeln("Parsing page <info>$currentPage</info>...");

                $grabber->setUrl("$siteSearchUrl?country=$country&p=$currentPage");
                $content = $grabber->getContent();
                if (!$content) {
                    $output->writeln('<error>Error</error>');
                    break;
                }

                $beachesJsonObject = json_decode($content);
                $totalPages = ceil((int)$beachesJsonObject->totalHits / 20);
                $currentPage++;

                foreach ($beachesJsonObject->pois as $beach) {
                    $beachJson = json_encode($beach);

                    /**
                     * @var Beach $beach
                     */

                    $beach = $serializer->deserialize($beachJson, 'BeachBundle\Document\Beach', 'json');
                    $dm->persist($beach);
                }
            }
        }
        $dm->flush();


    }

    private function getCountries()
    {
        return ["united-states-of-america","virgin-islands-(usa)","united-kingdom","afghanistan","albania","algeria","american-samoa","andorra","angola","anguilla","antarctica","antigua-and-barbuda","argentina","armenia","aruba","australia","austria","azerbaijan","bahamas","bahrain","bangladesh","barbados","belarus","belgium","belize","benin","bermuda","bhutan","bolivia","bosnia-and-herzegovina","botswana","brazil","brunei","bulgaria","burkina-faso","burundi","cambodia","cameroon","canada","cape-verde","cayman-islands","central-african-rep.","chad","chile","china","christmas-island","cocos-islands","colombia","comoros","congo","congo-dem.-rep.-of","cook-islands","costa-rica","croatia","cuba","cyprus","czech-republic","denmark","djibouti","dominica","dominican-rep.","ecuador","egypt","el-salvador","equatorial-guinea","eritrea","estonia","ethiopia","faroe-islands","falkland-islands","fiji","finland","france","french-guiana","french-polynesia","gabon","gambia","georgia","germany","ghana","gibraltar","greece","greenland","grenada","guadeloupe","guam","guatemala","guinea","guinea-bissau","guyana","haiti","honduras","hong-kong","hungary","iceland","india","indonesia","iran","iraq","ireland","israel","italy","jamaica","japan","jordan","kazakhstan","kenya","kiribati","korea-north","kuwait","kyrgyzstan","laos","latvia","lebanon","lesotho","liberia","libya","liechtenstein","lithuania","luxembourg","macao","macedonia","madagascar","malawi","malaysia","maldives","mali","malta","marshall-islands","martinique","mauritania","mauritius","mayotte","mexico","federated-states-of-micronesia","moldova","monaco","mongolia","montserrat","morocco","mozambique","myanmar-(burma)","namibia","nauru","nepal","netherlands","netherlands-antilles","new-caledonia","new-zealand","nicaragua","niger","nigeria","niue","norfolk-island","norway","oman","pakistan","palau","palestine","panama","papua-new-guinea","paraguay","peru","philippines","poland","portugal","puerto-rico","qatar","reunion","romania","russia","rwanda","st-helena","saint-kitts-and-nevis-anguilla","st-lucia","saint-pierre-and-miquelon","saint-vincent-and-grenadines","san-marino","san-tome-amp;-principe","saudi-arabia","senegal","serbia","seychelles","sierra-leone","singapore","slovakia","slovenia","solomon-islands","somalia","south-africa","spain","sri-lanka","sudan","suriname","swaziland","sweden","switzerland","syria","taiwan","tajikistan","tanzania","thailand","togo","tonga","trinidad-amp;-tobago","tunisia","turkey","turkmenistan","turks-amp;-caicos","tuvalu","uganda","ukraine","united-arab-emirates","uruguay","uzbekistan","vanuatu","vatican-city","venezuela","vietnam","wallis-amp;-futuna","yemen","zambia","zimbabwe","aland-islands","french-southern-and-antarctic-lands","french-southern-and-antarctic-lands","guernsey","isle-of-man","jersey","montenegro","northern-mariana-is","pitcairn-islands","samoa","svalbard-and-jan-mayen-islands","timor-leste","tokelau","united-states-minor-outlying-islands","virgin-islands-(british)","western-sahara","guernsey","south-georgia-and-the-south-sandwich-isl","south-korea"];
    }
}