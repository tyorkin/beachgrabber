<?php
namespace BeachBundle\Model;

class UsageTime
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $amountOfPeople;
}