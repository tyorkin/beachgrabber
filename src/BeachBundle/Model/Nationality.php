<?php
namespace BeachBundle\Model;

class Nationality
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $rating;
}