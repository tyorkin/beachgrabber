<?php
namespace BeachBundle\Model;

class BeachDirection
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $detailedDescription;

    /**
     * @var string
     */
    protected $difficultyOfJourney;

    /**
     * @var integer
     */
    protected $difficultyParkingToBeach;
}