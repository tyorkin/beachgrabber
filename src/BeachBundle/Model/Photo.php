<?php
namespace BeachBundle\Model;

class Photo
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $url;
}