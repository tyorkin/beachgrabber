<?php
namespace BeachBundle\Model;

class CarParkingSituation
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $parkingDescription;

    /**
     * @var integer
     */
    protected $difficultyFindingParking;
}