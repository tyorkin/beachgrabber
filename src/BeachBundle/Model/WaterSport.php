<?php
namespace BeachBundle\Model;

class WaterSport
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $rating;
}