<?php
namespace BeachBundle\Model;

class LifestyleInfo
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $rating;
}