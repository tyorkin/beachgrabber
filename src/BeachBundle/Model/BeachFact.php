<?php

namespace BeachBundle\Model;

use BeachBundle\Document\Fact;

class BeachFact
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Fact[]
     */
    protected $facts;
}