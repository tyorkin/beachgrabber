<?php
namespace BeachBundle\Model;

use BeachBundle\Document\BeachDirection;
use BeachBundle\Document\BeachFact;
use BeachBundle\Document\CarParkingSituation;
use BeachBundle\Document\Food;
use BeachBundle\Document\LifestyleInfo;
use BeachBundle\Document\Nationality;
use BeachBundle\Document\Photo;
use BeachBundle\Document\Risk;
use BeachBundle\Document\UsageTime;
use BeachBundle\Document\WaterSport;

class Beach
{
    /**
     * @var string
     */
    protected $_id;

    /**
     * @var string
     */
    protected $area;

    /**
     * @var double
     */
    protected $geoLocationLon;

    /**
     * @var integer
     */
    protected $interviewEntspannung08;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var integer
     */
    protected $interviewWassersport08;

    /**
     * @var string
     */
    protected $localId;

    /**
     * @var integer
     */
    protected $interviewRuhe08;

    /**
     * @var string
     */
    protected $interviewFamilien08;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var integer
     */
    protected $interviewParty08;

    /**
     * @var string
     */
    protected $imageDefaultTitle;

    /**
     * @var string
     */
    protected $strandStatement;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $urlKey;

    /**
     * @var double
     */
    protected $geoLocationLat;

    /**
     * @var string
     */
    protected $imageDefaultUrl;

    /**
     * @var string
     */
    protected $basicTeam1;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var boolean
     */
    protected $strandTipp;

    /**
     * @var boolean
     */
    protected $strandBeliebterurlaubsort;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $thumbnail;

    /**
     * @var Integer
     */
    protected $score;

    /**
     * @var string
     */
    protected $mainCategory;

    /**
     * @var string
     */
    protected $infoWindow;

    /**
     * @var Photo[]
     */
    protected $photos;

    /**
     * @var BeachFact[]
     */
    protected $beachFacts;

    /**
     * @var LifestyleInfo[]
     */
    protected $lifestyleInfo;

    /**
     * @var Nationality[]
     */
    protected $nationalities;

    /**
     * @var UsageTime[]
     */
    protected $usageTimes;

    /**
     * @var AgeRange[]
     */
    protected $ageRanges;

    /**
     * @var BeachDirection[]
     */
    protected $directionsToTheBeach;

    /**
     * @var CarParkingSituation
     */
    protected $carParkingSituation;

    /**
     * @var Risk[]
     */
    protected $risks;

    /**
     * @var Food[]
     */
    protected $food;

    /**
     * @var WaterSport[]
     */
    protected $waterSports;
}