<?php
namespace BeachBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;

/**
 * @MongoDb\EmbeddedDocument()
 */
class CarParkingSituation
{
    /**
     * @MongoDb\Id()
     */
    protected $id;

    /**
     * @MongoDb\String()
     */
    protected $parkingDescription;

    /**
     * @MongoDb\Integer()
     */
    protected $difficultyFindingParking;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParkingDescription()
    {
        return $this->parkingDescription;
    }

    /**
     * @param mixed $parkingDescription
     *
     * @return self;
     */
    public function setParkingDescription($parkingDescription)
    {
        $this->parkingDescription = $parkingDescription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDifficultyFindingParking()
    {
        return $this->difficultyFindingParking;
    }

    /**
     * @param mixed $difficultyFindingParking
     *
     * @return self;
     */
    public function setDifficultyFindingParking($difficultyFindingParking)
    {
        $this->difficultyFindingParking = $difficultyFindingParking;

        return $this;
    }


}