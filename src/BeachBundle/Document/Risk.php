<?php
namespace BeachBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;

/**
 * @MongoDb\EmbeddedDocument()
 */
class Risk
{
    /**
     * @MongoDb\Id()
     */
    protected $id;

    /**
     * @MongoDb\String()
     */
    protected $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}