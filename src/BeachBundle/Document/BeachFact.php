<?php

namespace BeachBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;

/**
 * @MongoDb\EmbeddedDocument()
 */
class BeachFact
{
    /**
     * @MongoDb\Id()
     */
    protected $id;

    /**
     * @MongoDb\String()
     */
    protected $name;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\Fact")
     */
    protected $facts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacts()
    {
        return $this->facts;
    }

    /**
     * @param Fact[] $facts
     *
     * @return self
     */
    public function setFacts($facts)
    {
        $this->facts = $facts;

        return $this;
    }


}