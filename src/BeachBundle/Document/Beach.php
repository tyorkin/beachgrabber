<?php
namespace BeachBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDb\Document(
 *     collection="beach",
 *     repositoryClass="BeachBundle\Repository\BeachRepository"
 * )
 */
class Beach
{
    /**
     * @MongoDb\Id()
     */
    protected $_id;

    /**
     * @MongoDb\String()
     */
    protected $area;

    /**
     * @MongoDb\Float()
     */
    protected $geoLocationLon;

    /**
     * @MongoDb\Integer()
     */
    protected $interviewEntspannung08;

    /**
     * @MongoDb\String()
     */
    protected $country;

    /**
     * @MongoDb\Integer()
     */
    protected $interviewWassersport08;

    /**
     * @MongoDb\String()
     */
    protected $localId;

    /**
     * @MongoDb\Integer()
     */
    protected $interviewRuhe08;

    /**
     * @MongoDb\Integer()
     */
    protected $interviewFamilien08;

    /**
     * @MongoDb\String()
     */
    protected $city;

    /**
     * @MongoDb\Integer()
     */
    protected $interviewParty08;

    /**
     * @MongoDb\String()
     */
    protected $imageDefaultTitle;

    /**
     * @MongoDb\String()
     */
    protected $strandStatement;

    /**
     * @MongoDb\String()
     */
    protected $title;

    /**
     * @MongoDb\String()
     */
    protected $urlKey;

    /**
     * @MongoDb\Float()
     */
    protected $geoLocationLat;

    /**
     * @MongoDb\String()
     */
    protected $imageDefaultUrl;

    /**
     * @MongoDb\String()
     */
    protected $basicTeam1;

    /**
     * @MongoDb\String()
     */
    protected $id;

    /**
     * @MongoDb\Boolean()
     */
    protected $strandTipp;

    /**
     * @MongoDb\Boolean()
     */
    protected $strandBeliebterurlaubsort;

    /**
     * @MongoDb\String()
     */
    protected $url;

    /**
     * @MongoDb\String()
     */
    protected $thumbnail;

    /**
     * @MongoDb\Integer()
     */
    protected $score;

    /**
     * @MongoDb\String()
     */
    protected $mainCategory;

    /**
     * @MongoDb\String()
     */
    protected $infoWindow;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\Photo")
     */
    protected $photos;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\BeachFact")
     */
    protected $beachFacts;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\LifestyleInfo")
     */
    protected $lifestyleInfo;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\Nationality")
     */
    protected $nationalities;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\UsageTime")
     */
    protected $usageTimes;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\AgeRange")
     */
    protected $ageRanges;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\BeachDirection")
     */
    protected $directionsToTheBeach;

    /**
     * @MongoDb\EmbedOne(targetDocument="BeachBundle\Document\CarParkingSituation")
     */
    protected $carParkingSituation;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\Risk")
     */
    protected $risks;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\Food")
     */
    protected $food;

    /**
     * @MongoDb\EmbedMany(targetDocument="BeachBundle\Document\WaterSport")
     */
    protected $waterSports;

    /**
     * @MongoDb\Timestamp()
     * @Gedmo\Timestampable(on="update")
     */
    protected $lastUpdate;

    /**
     * @MongoDb\Timestamp()
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
        $this->beachFacts = new ArrayCollection();
        $this->lifestyleInfo = new ArrayCollection();
        $this->nationalities = new ArrayCollection();
        $this->usageTimes = new ArrayCollection();
        $this->ageRanges = new ArrayCollection();
        $this->directionsToTheBeach = new ArrayCollection();
        $this->risks = new ArrayCollection();
        $this->food = new ArrayCollection();
        $this->waterSports = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $thumbnail
     *
     * @return self
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function get_Id()
    {
        return $this->_id;
    }

    /**
     * @return mixed
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     *
     * @return self
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGeoLocationLon()
    {
        return $this->geoLocationLon;
    }

    /**
     * @param mixed $geoLocationLon
     *
     * @return self
     */
    public function setGeoLocationLon($geoLocationLon)
    {
        $this->geoLocationLon = $geoLocationLon;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterviewEntspannung08()
    {
        return $this->interviewEntspannung08;
    }

    /**
     * @param mixed $interviewEntspannung08
     *
     * @return self
     */
    public function setInterviewEntspannung08($interviewEntspannung08)
    {
        $this->interviewEntspannung08 = $interviewEntspannung08;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterviewWassersport08()
    {
        return $this->interviewWassersport08;
    }

    /**
     * @param mixed $interviewWassersport08
     *
     * @return self
     */
    public function setInterviewWassersport08($interviewWassersport08)
    {
        $this->interviewWassersport08 = $interviewWassersport08;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * @param mixed $localId
     *
     * @return self
     */
    public function setLocalId($localId)
    {
        $this->localId = $localId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterviewRuhe08()
    {
        return $this->interviewRuhe08;
    }

    /**
     * @param mixed $interviewRuhe08
     *
     * @return self
     */
    public function setInterviewRuhe08($interviewRuhe08)
    {
        $this->interviewRuhe08 = $interviewRuhe08;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterviewFamilien08()
    {
        return $this->interviewFamilien08;
    }

    /**
     * @param mixed $interviewFamilien08
     *
     * @return self
     */
    public function setInterviewFamilien08($interviewFamilien08)
    {
        $this->interviewFamilien08 = $interviewFamilien08;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInterviewParty08()
    {
        return $this->interviewParty08;
    }

    /**
     * @param mixed $interviewParty08
     *
     * @return self
     */
    public function setInterviewParty08($interviewParty08)
    {
        $this->interviewParty08 = $interviewParty08;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageDefaultTitle()
    {
        return $this->imageDefaultTitle;
    }

    /**
     * @param mixed $imageDefaultTitle
     *
     * @return self
     */
    public function setImageDefaultTitle($imageDefaultTitle)
    {
        $this->imageDefaultTitle = $imageDefaultTitle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrandStatement()
    {
        return $this->strandStatement;
    }

    /**
     * @param mixed $strandStatement
     *
     * @return self
     */
    public function setStrandStatement($strandStatement)
    {
        $this->strandStatement = $strandStatement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrlKey()
    {
        return $this->urlKey;
    }

    /**
     * @param mixed $urlKey
     *
     * @return self
     */
    public function setUrlKey($urlKey)
    {
        $this->urlKey = $urlKey;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGeoLocationLat()
    {
        return $this->geoLocationLat;
    }

    /**
     * @param mixed $geoLocationLat
     *
     * @return self
     */
    public function setGeoLocationLat($geoLocationLat)
    {
        $this->geoLocationLat = $geoLocationLat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageDefaultUrl()
    {
        return $this->imageDefaultUrl;
    }

    /**
     * @param mixed $imageDefaultUrl
     *
     * @return self
     */
    public function setImageDefaultUrl($imageDefaultUrl)
    {
        $this->imageDefaultUrl = $imageDefaultUrl;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBasicTeam1()
    {
        return $this->basicTeam1;
    }

    /**
     * @param mixed $basicTeam1
     *
     * @return self
     */
    public function setBasicTeam1($basicTeam1)
    {
        $this->basicTeam1 = $basicTeam1;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrandTipp()
    {
        return $this->strandTipp;
    }

    /**
     * @param mixed $strandTipp
     *
     * @return self
     */
    public function setStrandTipp($strandTipp)
    {
        $this->strandTipp = $strandTipp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrandBeliebterurlaubsort()
    {
        return $this->strandBeliebterurlaubsort;
    }

    /**
     * @param mixed $strandBeliebterurlaubsort
     *
     * @return self
     */
    public function setStrandBeliebterurlaubsort($strandBeliebterurlaubsort)
    {
        $this->strandBeliebterurlaubsort = $strandBeliebterurlaubsort;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     *
     * @return self
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMainCategory()
    {
        return $this->mainCategory;
    }

    /**
     * @param mixed $mainCategory
     *
     * @return self
     */
    public function setMainCategory($mainCategory)
    {
        $this->mainCategory = $mainCategory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInfoWindow()
    {
        return $this->infoWindow;
    }

    /**
     * @param mixed $infoWindow
     *
     * @return self
     */
    public function setInfoWindow($infoWindow)
    {
        $this->infoWindow = $infoWindow;

        return $this;
    }

    /**
     * @return Photo[]
     */
    public function getPhotos()
    {
        return $this->photos->toArray();
    }

    /**
     * @param Photo[] $photos
     *
     * @return self
     */
    public function setPhotos(array $photos)
    {
        $this->photos = new ArrayCollection($photos);

        return $this;
    }

    /**
     * @param Photo $photo
     *
     * @return self
     */
    public function addPhoto(Photo $photo)
    {
        $this->photos->add($photo);

        return $this;
    }

    /**
     * @param Photo $photo
     *
     * @return void
     */
    public function removePhoto(Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * @return BeachFact[]
     */
    public function getBeachFacts()
    {
        return $this->beachFacts->toArray();
    }

    /**
     * @param BeachFact[] $beachFacts
     *
     * @return self
     */
    public function setBeachFacts(array $beachFacts)
    {
        $this->beachFacts = new ArrayCollection($beachFacts);

        return $this;
    }

    /**
     * @param BeachFact $beachFact
     *
     * @return self
     */
    public function addBeachFact(BeachFact $beachFact)
    {
        $this->beachFacts->add($beachFact);

        return $this;
    }

    /**
     * @param BeachFact $beachFact
     *
     * @return self
     */
    public function removeBeachFact(BeachFact $beachFact)
    {
        $this->beachFacts->removeElement($beachFact);

        return $this;
    }

    /**
     * @return LifestyleInfo[]
     */
    public function getLifestyleInfo()
    {
        return $this->lifestyleInfo->toArray();
    }

    /**
     * @param LifestyleInfo[] $lifestyleInfo
     *
     * @return self
     */
    public function setLifestyleInfo(array $lifestyleInfo)
    {
        $this->lifestyleInfo = new ArrayCollection($lifestyleInfo);

        return $this;
    }

    /**
     * @param LifestyleInfo $lifestyleInfo
     *
     * @return self
     */
    public function addLifestyleInfo(LifestyleInfo $lifestyleInfo)
    {
        $this->lifestyleInfo->add($lifestyleInfo);
    }

    /**
     * @param LifestyleInfo $lifestyleInfo
     *
     * @return void
     */
    public function removeLifestyleInfo(LifestyleInfo $lifestyleInfo)
    {
        $this->lifestyleInfo->removeElement($lifestyleInfo);
    }

    /**
     * @return Nationality[]
     */
    public function getNationalities()
    {
        return $this->nationalities->toArray();
    }

    /**
     * @param Nationality[] $nationalities
     *
     * @return self
     */
    public function setNationalities(array $nationalities)
    {
        $this->nationalities = new ArrayCollection($nationalities);

        return $this;
    }

    /**
     * @param Nationality $nationality
     *
     * @return self
     */
    public function addNationality($nationality)
    {
        $this->nationalities->add($nationality);

        return $this;
    }

    /**
     * @param Nationality $nationality
     *
     * @return void
     */
    public function removeNationality($nationality)
    {
        $this->nationalities->removeElement($nationality);
    }

    /**
     * @return UsageTime[]
     */
    public function getUsageTimes()
    {
        return $this->usageTimes->toArray();
    }

    /**
     * @param UsageTime[] $usageTimes
     *
     * @return self
     */
    public function setUsageTimes(array $usageTimes)
    {
        $this->usageTimes = new ArrayCollection($usageTimes);

        return $this;
    }

    /**
     * @param UsageTime $usageTime
     *
     * @return self
     */
    public function addUsageTime($usageTime)
    {
        $this->usageTimes->add($usageTime);

        return $this;
    }

    /**
     * @param UsageTime $usageTime
     *
     * @return void
     */
    public function removeUsageTime($usageTime)
    {
        $this->usageTimes->removeElement($usageTime);
    }

    /**
     * @return AgeRange[]
     */
    public function getAgeRanges()
    {
        return $this->ageRanges->toArray();
    }

    /**
     * @param AgeRange[] $ageRanges
     *
     * @return self
     */
    public function setAgeRanges(array $ageRanges)
    {
        $this->ageRanges = new ArrayCollection($ageRanges);

        return $this;
    }

    /**
     * @param AgeRange $ageRange
     *
     * @return self
     */
    public function addAgeRange($ageRange)
    {
        $this->ageRanges->add($ageRange);

        return $this;
    }

    /**
     * @param AgeRange $ageRange
     *
     * @return void
     */
    public function removeAgeRange($ageRange)
    {
        $this->ageRanges->removeElement($ageRange);
    }

    /**
     * @return BeachDirection[]
     */
    public function getDirectionsToTheBeach()
    {
        return $this->directionsToTheBeach->toArray();
    }

    /**
     * @param BeachDirection[] $directionsToTheBeach
     *
     * @return self
     */
    public function setDirectionsToTheBeach(array $directionsToTheBeach)
    {
        $this->directionsToTheBeach = new ArrayCollection($directionsToTheBeach);

        return $this;
    }

    /**
     * @param BeachDirection $directionToTheBeach
     *
     * @return self
     */
    public function addDirectionToTheBeach(BeachDirection $directionToTheBeach)
    {
        $this->directionsToTheBeach->add($directionToTheBeach);

        return $this;
    }

    /**
     * @param BeachDirection $directionToTheBeach
     *
     * @return void
     */
    public function removeDirectionToTheBeach(BeachDirection $directionToTheBeach)
    {
        $this->directionsToTheBeach->removeElement($directionToTheBeach);
    }

    /**
     * @return CarParkingSituation
     */
    public function getCarParkingSituation()
    {
        return $this->carParkingSituation;
    }

    /**
     * @param CarParkingSituation $carParkingSituation
     *
     * @return self
     */
    public function setCarParkingSituation($carParkingSituation)
    {
        $this->carParkingSituation = $carParkingSituation;

        return $this;
    }

    /**
     * @return Risk[]
     */
    public function getRisks()
    {
        return $this->risks->toArray();
    }

    /**
     * @param Risk[] $risks
     *
     * @return self
     */
    public function setRisks(array $risks)
    {
        $this->risks = new ArrayCollection($risks);

        return $this;
    }

    /**
     * @param Risk $risk
     *
     * @return self
     */
    public function addRisk($risk)
    {
        $this->risks->add($risk);

        return $this;
    }

    /**
     * @param Risk $risk
     *
     * @return void
     */
    public function removeRisk($risk)
    {
        $this->risks->removeElement($risk);
    }

    /**
     * @return Food[]
     */
    public function getFood()
    {
        return $this->food->toArray();
    }

    /**
     * @param Food[] $food
     *
     * @return self
     */
    public function setFood(array $food)
    {
        $this->food = new ArrayCollection($food);

        return $this;
    }

    /**
     * @param Food $food
     *
     * @return self
     */
    public function addFood(Food$food)
    {
        $this->food->add($food);

        return $this;
    }

    /**
     * @param Food $food
     *
     * @return void
     */
    public function removeFood(Food $food)
    {
        $this->food->removeElement($food);
    }

    /**
     * @return WaterSport[]
     */
    public function getWaterSports()
    {
        return $this->waterSports->toArray();
    }

    /**
     * @param WaterSport[] $waterSports
     *
     * @return self
     */
    public function setWaterSports(array $waterSports)
    {
        $this->waterSports = new ArrayCollection($waterSports);

        return $this;
    }

    /**
     * @param WaterSport $waterSport
     *
     * @return self
     */
    public function addWaterSport($waterSport)
    {
        $this->waterSports->add($waterSport);

        return $this;
    }

    /**
     * @param WaterSport $waterSport
     *
     * @return void
     */
    public function removeWaterSport($waterSport)
    {
        $this->waterSports->removeElement($waterSport);
    }

    /**
     * @return \MongoTimestamp
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @return \MongoTimestamp
     */
    public function getCreated()
    {
        return $this->created;
    }
}