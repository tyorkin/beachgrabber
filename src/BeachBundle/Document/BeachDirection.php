<?php
namespace BeachBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;

/**
 * @MongoDb\EmbeddedDocument()
 */
class BeachDirection
{
    /**
     * @MongoDb\Id()
     */
    protected $id;

    /**
     * @MongoDb\String()
     */
    protected $detailedDescription;

    /**
     * @MongoDb\String()
     */
    protected $difficultyOfJourney;

    /**
     * @MongoDb\Integer()
     */
    protected $difficultyParkingToBeach;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDetailedDescription()
    {
        return $this->detailedDescription;
    }

    /**
     * @param mixed $detailedDescription
     *
     * @return self
     */
    public function setDetailedDescription($detailedDescription)
    {
        $this->detailedDescription = $detailedDescription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDifficultyOfJourney()
    {
        return $this->difficultyOfJourney;
    }

    /**
     * @param mixed $difficultyOfJourney
     *
     * @return self
     */
    public function setDifficultyOfJourney($difficultyOfJourney)
    {
        $this->difficultyOfJourney = $difficultyOfJourney;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDifficultyParkingToBeach()
    {
        return $this->difficultyParkingToBeach;
    }

    /**
     * @param mixed $difficultyParkingToBeach
     *
     * @return self
     */
    public function setDifficultyParkingToBeach($difficultyParkingToBeach)
    {
        $this->difficultyParkingToBeach = $difficultyParkingToBeach;

        return $this;
    }


}