<?php
namespace BeachBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDb;

/**
 * @MongoDb\EmbeddedDocument()
 */
class UsageTime
{
    /**
     * @MongoDb\Id()
     */
    protected $id;

    /**
     * @MongoDb\String()
     */
    protected $name;

    /**
     * @MongoDb\Integer()
     */
    protected $amountOfPeople;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     *
     * @return self
     */
    public function getAmountOfPeople()
    {
        return $this->amountOfPeople;
    }

    /**
     * @param mixed $amountOfPeople
     *
     * @return self
     */
    public function setAmountOfPeople($amountOfPeople)
    {
        $this->amountOfPeople = $amountOfPeople;

        return $this;
    }


}